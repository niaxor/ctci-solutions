﻿using System;
using System.Collections.Generic;

namespace Partition
{
    public class LinkedList <T> where T : IComparable<T>
    {
        public LinkedListNode<T> First { get; set; }
        public LinkedListNode<T> Last { get; set; }

        public LinkedList(IEnumerable<T> collection) 
        {
            var node = new LinkedListNode<T>();
            foreach (var item in collection)
            {
                if (First == null)
                {
                    First = new LinkedListNode<T>() { Value = item };
                    node = First;
                }
                else
                {
                    node.Next = new LinkedListNode<T>() { Value = item };
                    node = node.Next;
                }
            }
            Last = node;
        }

        public void Partition(T value)
        {
            if (First == null)
                throw new InvalidOperationException("Linked list is empty.");

            LinkedListNode<T> currentLeftNode = null;
            LinkedListNode<T> currentRightNode = null;
            LinkedListNode<T> entryLeftNode = null;
            LinkedListNode<T> entryRightNode = null;
            var searchNode = First;

            // Create partitions
            while (searchNode != null)
            {
                if (searchNode.Value.CompareTo(value) >= 0)
                {
                    if (currentRightNode == null)
                    {
                        currentRightNode = searchNode;
                        entryRightNode = searchNode;
                    }
                    else
                    {
                        currentRightNode.Next = searchNode;
                        currentRightNode = currentRightNode.Next;
                    }
                }
                else
                {
                    if (currentLeftNode == null)
                    {
                        currentLeftNode = searchNode;
                        entryLeftNode = searchNode;
                    }
                    else
                    {
                        currentLeftNode.Next = searchNode;
                        currentLeftNode = currentLeftNode.Next;
                    }
                }

                searchNode = searchNode.Next;
            }

            // First and last
            First = entryLeftNode == null ? entryRightNode : entryLeftNode;
            Last = currentRightNode == null ? currentLeftNode : currentRightNode;

            // Piece together partitions if necessary
            if (entryRightNode != null && currentLeftNode != null)
            {
                currentLeftNode.Next = entryRightNode;
            }

            // Trim off end
            Last.Next = null;
        }

        public override bool Equals(object obj)
        {
            // Must be linked list
            if (!(obj is LinkedList<T>))
                return false;

            var listToCheck = obj as LinkedList<T>;
            // Check for non-matching First/Last properties
            if (!Object.Equals(this.First, listToCheck.First) || !Object.Equals(this.Last, listToCheck.Last))
                return false;

            var node1 = this.First;
            var node2 = listToCheck.First;

            // Check each nodes value is identical 
            while (node1 != null && node2 != null)
            {
                if (!node1.Value.Equals(node2.Value))
                    return false;

                node1 = node1.Next;
                node2 = node2.Next;
            }

            // Check length is equal
            return node1 == null && node2 == null;
        }

        public override int GetHashCode()
        {
            // Hashing performance is irrelevant in this example
            return 0;
        }
    }
}
