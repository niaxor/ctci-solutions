﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Partition.Tests
{
    [TestClass]
    public class PartitionTests
    {
        [TestMethod]
        [DataRow(new int[] { 4, 3, 2, 1 }, new int[] { 1, 4, 3, 2 }, 2)]
        [DataRow(new int[] { 1, 1, 2, 1 }, new int[] { 1, 1, 2, 1}, 1)]
        [DataRow(new int[] { 3, 3, 3, 1 }, new int[] { 1, 3, 3, 3 }, 2)]
        [DataRow(new int[] { 1, 2, 1, 2 }, new int[] { 1, 2, 1, 2 }, 1)]
        public void Partition_ValueExists_ReturnsExpectedResult(int[] inputArr, int[] expectedOutputArr, int sortValue)
        {
            LinkedList<int> input = new LinkedList<int>(inputArr);
            LinkedList<int> expectedOutput = new LinkedList<int>(expectedOutputArr);
            input.Partition(sortValue);
            Assert.AreEqual(expectedOutput, input);
        }

        [TestMethod]
        [DataRow(new int[] { 1 }, 2)]
        [DataRow(new int[] { 4, 3, 2, 1 }, 1)]
        [DataRow(new int[] { 1, 1, 2, 1 }, 1)]
        [DataRow(new int[] { 3, 3, 3, 1 }, 1)]
        public void Partition_NothingToSort_ReturnsInput(int[] inputArr, int sortValue)
        {
            LinkedList<int> input = new LinkedList<int>(inputArr);
            LinkedList<int> expectedOutput = new LinkedList<int>(inputArr);
            input.Partition(sortValue);
            Assert.AreEqual(expectedOutput, input);
        }
    }
}
