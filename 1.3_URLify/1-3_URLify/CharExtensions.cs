﻿namespace _1_3_URLify
{
    /// <summary>
    /// Write a method to replace all spaces in a string with '%20'. You may assume that the string has sufficient space at the 
    /// end to hold the additional characters, and that you are given the "true" length of the string.
    /// (Note: If implementing in Java, please use a character array so that you can perform this operation in place.) 
    /// </summary>
    public static class CharExtensions
    {
        public static char[] URLify(this char[] instance, int realLength)
        {
            char[] output = new char[instance.Length];
            int outputIndex = 0;
            int instanceIndex = 0;
            while (instanceIndex < realLength)
            {
                if (instance[instanceIndex] == ' ')
                {
                    output[outputIndex] = '%';
                    output[outputIndex + 1] = '2';
                    output[outputIndex + 2] = '0';
                    outputIndex += 3;
                }
                else
                {
                    output[outputIndex] = instance[instanceIndex];
                    outputIndex++;
                }
                instanceIndex++;
            }

            return output;
        }
    }
}
