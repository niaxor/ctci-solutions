﻿using System;
using _1_3_URLify;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace URLify.Tests
{
    [TestClass]
    public class CharExtensionTests
    {
        [TestMethod]
        [DataRow("ABCD", 4, "ABCD")]
        [DataRow("ABCD   ", 5, "ABCD%20")]
        [DataRow(" ABCD  ", 5, "%20ABCD")]
        [DataRow("AB CD  ", 5, "AB%20CD")]
        [DataRow("", 0, "")]
        [DataRow("   ", 1, "%20")]
        [DataRow("AB  CD    ", 6, "AB%20%20CD")]
        public void URLify_ExpectedLength(string inputStr, int realLength, string expectedOutputStr)
        {
            // Note: Test inputs declared as strings for readability
            var input = inputStr.ToCharArray();
            var expectedOutput = expectedOutputStr.ToCharArray();

            var output = input.URLify(realLength);
            Assert.AreEqual(output.Length, expectedOutput.Length);
        }

        [TestMethod]
        [DataRow("ABCD", 4, "ABCD")]
        [DataRow("ABCD   ", 5, "ABCD%20")]
        [DataRow(" ABCD  ", 5, "%20ABCD")]
        [DataRow("AB CD  ", 5, "AB%20CD")]
        [DataRow("", 0, "")]
        [DataRow("   ", 1, "%20")]
        [DataRow("AB  CD    ", 6, "AB%20%20CD")]
        public void URLify_ExpectedValue(string inputStr, int realLength, string expectedOutputStr)
        {
            // Note: Test inputs declared as strings for readability
            var input = inputStr.ToCharArray();
            var expectedOutput = expectedOutputStr.ToCharArray();

            var output = input.URLify(realLength);
            for (int i = 0; i < expectedOutput.Length; i++)
            {
                Assert.AreEqual(output[i], expectedOutput[i]);
            }
        }
    }
}
