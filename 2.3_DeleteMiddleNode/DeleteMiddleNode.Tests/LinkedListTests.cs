﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace DeleteMiddleNode.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        [DataRow(new int[] { 1, 2, 3, 4 }, new int[] { 1, 2, 4 }, 3)]
        [DataRow(new int[] { 1, 2, 3, 4 }, new int[] { 1, 3, 4 }, 2)]
        [DataRow(new int[] { 1, 1, 1, 1 }, new int[] { 1, 1, 1 }, 1)]
        public void DeleteMiddleNode_StandardInput_ResultsMatchOutput(int[] inputArr, int[] expectedOutputArr, int valueToRemove)
        {
            LinkedList<int> input = new LinkedList<int>(inputArr);
            LinkedList<int> expectedOutput = new LinkedList<int>(expectedOutputArr);
            var node = input.FindMiddleNode(valueToRemove);
            input.DeleteMiddleNode(node);
            Assert.AreEqual(expectedOutput, input);
        }

        [TestMethod]
        [DataRow(new int[] { 1 })]
        [DataRow(new int[] { 1, 1 })]
        public void DeleteMiddleNode_NoMiddleNode_ThrowsInvalidOperationException(int[] inputArr)
        {
            Assert.ThrowsException<InvalidOperationException>(() =>
            {
                LinkedList<int> input = new LinkedList<int>(inputArr);
                var node = new LinkedListNode<int>() { Value = 1 };
                input.DeleteMiddleNode(node);
            });
        }
    }
}
