﻿using System;

namespace DeleteMiddleNode.Tests
{
    public static class LinkedListExtensions
    {
        public static LinkedListNode<T> FindMiddleNode<T>(this LinkedList<T> list, T value)
        {
            if (list.First == null || list.First.Next == null ||
                list.First == list.Last || list.First.Next == list.Last)
                throw new InvalidOperationException("List has no middle nodes.");

            var node = list.First.Next;
            while (node != list.Last && node != null)
            {
                if (node.Value.Equals(value))
                    return node;

                node = node.Next;
            }

            throw new ArgumentException("List does not contain a middle node with the provided value.");
        }
    }
}
