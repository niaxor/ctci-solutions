﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Intersection.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        public void GetIntersectingNode_NoIntersectingNode_ReturnsNull()
        {
            var list1 = new LinkedList<int>();
            var list2 = new LinkedList<int>();

            list1.First = new LinkedListNode<int>(1);
            list1.First.Next = new LinkedListNode<int>(2);
            list1.First.Next.Next = new LinkedListNode<int>(3);

            list2.First = new LinkedListNode<int>(4);
            list2.First.Next = new LinkedListNode<int>(5);
            list2.First.Next.Next = new LinkedListNode<int>(6);

            var result = LinkedList<int>.GetIntersectingNode(list1, list2);
            Assert.IsNull(result);
        }

        [TestMethod]
        public void GetIntersectingNode_IdenticalLists_ReturnsFirstNode()
        {
            var list1 = new LinkedList<int>();
            var list2 = new LinkedList<int>();

            list1.First = new LinkedListNode<int>(1);
            list1.First.Next = new LinkedListNode<int>(2);
            list1.First.Next.Next = new LinkedListNode<int>(3);

            list2.First = list1.First;
            list2.First.Next = list1.First.Next;
            list2.First.Next.Next = list1.First.Next.Next;

            var result = LinkedList<int>.GetIntersectingNode(list1, list2);
            Assert.IsTrue(result == list1.First);
        }

        [TestMethod]
        public void GetIntersectingNode_DifferentSizedLists_ReturnsIntersectingNode()
        {
            var list1 = new LinkedList<int>();
            var list2 = new LinkedList<int>();

            list1.First = new LinkedListNode<int>(1);
            list1.First.Next = new LinkedListNode<int>(2);
            list1.First.Next.Next = new LinkedListNode<int>(3);

            list2.First = new LinkedListNode<int>(4);
            list2.First.Next = new LinkedListNode<int>(5);
            list2.First.Next.Next = list1.First.Next;

            var result = LinkedList<int>.GetIntersectingNode(list1, list2);
            Assert.IsTrue(result == list1.First.Next);
        }

        [TestMethod]
        public void GetIntersectingNode_NullList_ReturnsNull()
        {
            var list1 = new LinkedList<int>();
            var list2 = new LinkedList<int>();

            list1.First = new LinkedListNode<int>(1);
            list1.First.Next = new LinkedListNode<int>(2);
            list1.First.Next.Next = new LinkedListNode<int>(3);

            var result = LinkedList<int>.GetIntersectingNode(list1, list2);
            Assert.IsNull(result);
        }
    }
}