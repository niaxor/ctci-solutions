﻿using System;
using System.Collections.Generic;

namespace Intersection
{
    public class LinkedList<T> where T : IComparable<T>
    {
        public LinkedListNode<T> First { get; set; }

        public LinkedList() { }

        public static LinkedListNode<T> GetIntersectingNode(LinkedList<T> list1, LinkedList<T> list2)
        {
            int count1 = 0;
            int count2 = 0;
            var currentNode1 = list1.First;
            var currentNode2 = list2.First;
            LinkedListNode<T> lastNode1 = null;
            LinkedListNode<T> lastNode2 = null;

            // Validation
            if (currentNode1 == null || currentNode2 == null)
                return null;

            // Count nodes in both lists
            while (currentNode1 != null || currentNode2 != null)
            {
                if (currentNode1 != null)
                {
                    // Record last node of first list
                    if (currentNode1.Next == null)
                    {
                        lastNode1 = currentNode1;
                    }
                    count1++;
                    currentNode1 = currentNode1.Next;
                }
                if (currentNode2 != null)
                {                    
                    // Record last node of second list
                    if (currentNode2.Next == null)
                    {
                        lastNode2 = currentNode2;
                    }
                    count2++;
                    currentNode2 = currentNode2.Next;
                }
            }

            // Intersecting lists will share a final node
            if (lastNode1 != lastNode2)
                return null;

            var longListSearchNode = count1 > count2 ? list1.First : list2.First;
            var shortListSearchNode = count1 > count2 ? list2.First : list1.First;

            // Move long list search node to correct starting position
            for (int i = 0; i < Math.Abs(count2 - count1); i++)
            {
                longListSearchNode = longListSearchNode.Next;
            }

            // Find intersecting node
            while (shortListSearchNode != null && longListSearchNode != null)
            {
                if (shortListSearchNode == longListSearchNode)
                    return shortListSearchNode;

                longListSearchNode = longListSearchNode.Next;
                shortListSearchNode = shortListSearchNode.Next;
            }

            return null;
        }

        public override int GetHashCode()
        {
            // Hashing performance is irrelevant in this example
            return 0;
        }
    }
}
