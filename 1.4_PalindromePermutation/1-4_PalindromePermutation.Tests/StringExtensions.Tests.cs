﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _1_4_PalindromePermutation.Tests
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        [DataRow("abcd abcd", true)]
        [DataRow("abc d abcd", true)]
        [DataRow("a", true)]
        [DataRow("hello", false)]
        [DataRow("hi", false)]
        [DataRow("abcd e", false)]
        [DataRow("Tact Coa", true)]
        public void IsPalindromePermutation_ExpectedValue(string input, bool expectedResult)
        {
            Assert.AreEqual(input.IsPalindromePermutation(), expectedResult);
        }
    }
}
