﻿using System.Collections.Generic;

namespace _1_4_PalindromePermutation
{
    public static class StringExtensions
    {
        /// <summary>
        /// An O(n) solution for determining if a phrase is a palindrome permutation
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static bool IsPalindromePermutation(this string instance)
        {
            int realCharacterCount = 0; // How many non-whitespace characters
            int oddCharacters = 0;

            // Ignore case
            instance = instance.ToLower();
            Dictionary<char, int> characterCounter = new Dictionary<char, int>();
            for (int i = 0; i < instance.Length; i++)
            {
                // Ignore whitespace
                if (instance[i] == ' ')
                    continue;

                if (characterCounter.ContainsKey(instance[i]))
                    characterCounter[instance[i]]++;
                else
                    characterCounter.Add(instance[i], 1);

                realCharacterCount++;
            }

            // Count amount of characters with odd character counts
            foreach (var kvp in characterCounter)
            {
                if (kvp.Value % 2 == 1)
                {
                    oddCharacters++;
                }
            }
            bool isEven = realCharacterCount % 2 == 0;
            bool singleOddCharacter = oddCharacters == 1;
            // If even length, there can be no odd characters; if odd, there can only be one odd character
            bool isPalindromePermutation = ((isEven && oddCharacters == 0) || (!isEven && singleOddCharacter));

            return isPalindromePermutation;
        }
    }
}
