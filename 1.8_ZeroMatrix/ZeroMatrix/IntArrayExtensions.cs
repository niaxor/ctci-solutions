﻿namespace ZeroMatrix
{
    public static class IntArrayExtensions
    {
        public static void Zero(this int[][] instance)
        {
            bool[][] visitedCells = new bool[instance.Length][];
            for (int i = 0; i < visitedCells.Length; i++)
            {
                visitedCells[i] = new bool[instance[i].Length];
            }

            for (int y = 0; y < instance.Length; y++)
            {
                for (int x = 0; x < instance[y].Length; x++)
                {
                    if (visitedCells[y][x] == true)
                        continue;

                    if (instance[y][x] == 0)
                    {
                        visitedCells[y][x] = true;
                        ZeroCell(instance, visitedCells, x + 1, y, 1, 0);
                        ZeroCell(instance, visitedCells, x - 1, y, -1, 0);
                        ZeroCell(instance, visitedCells, x, y + 1, 0, 1);
                        ZeroCell(instance, visitedCells, x, y - 1, 0, -1);
                    }
                }
            }
        }

        private static void ZeroCell(int[][] matrix, bool[][] visitedCells, int x, int y, int xVelocity, int yVelocity)
        {
            // Outside bounds of matrix OR visited already
            if (y >= visitedCells.Length || y < 0 ||
                x >= visitedCells[y].Length || x < 0 || visitedCells[y][x])
                return;

            visitedCells[y][x] = true;
            // Zero all surrounding cells if found new zero cell
            if (matrix[y][x] == 0)
            {
                ZeroCell(matrix, visitedCells, x + 1, y, 1, 0);
                ZeroCell(matrix, visitedCells, x - 1, y, -1, 0);
                ZeroCell(matrix, visitedCells, x, y + 1, 0, 1);
                ZeroCell(matrix, visitedCells, x, y - 1, 0, -1);
            }
            matrix[y][x] = 0;

            ZeroCell(matrix, visitedCells, x + xVelocity, y + yVelocity, xVelocity, yVelocity);
        }
    }
}
