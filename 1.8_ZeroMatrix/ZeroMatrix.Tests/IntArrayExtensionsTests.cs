﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ZeroMatrix.Tests
{
    [TestClass]
    public class IntArrayExtensionsTests
    {
        public int[][] DecodeMatrix(string encodedString)
        {
            string[] rows = encodedString.Split('\n');
            int[][] output = new int[rows.Length][];
            for (int y = 0; y < rows.Length; y++)
            {
                string[] row = rows[y].Split(' ');
                output[y] = new int[row.Length];
                for (int x = 0; x < row.Length; x++)
                {
                    output[y][x] = int.Parse(row[x]);
                }
            }

            return output;
        }

        [TestMethod]
        [DataRow(1, 1, "1", "1")]
        [DataRow(1, 1, "0", "0")]
        [DataRow(2, 2, "1 2\n3 4", "1 2\n3 4")]
        [DataRow(2, 2, "1 0\n1 1", "0 0\n1 0")]
        [DataRow(3, 3, "1 0 1\n1 1 1\n1 1 1", "0 0 0\n1 0 1\n1 0 1")]
        [DataRow(3, 3, "1 0 1\n0 1 1\n1 1 0", "0 0 0\n0 0 0\n0 0 0")]
        [DataRow(4, 4, "1 1 1 1\n1 0 1 1\n1 1 1 1\n1 1 1 1", "1 0 1 1\n0 0 0 0\n1 0 1 1\n1 0 1 1")]
        public void Zero_ExpectedOutput(int m, int n, string inputEncoded, string expectedOutputEncoded)
        {
            int[][] expectedOutput = DecodeMatrix(expectedOutputEncoded);
            int[][] input = DecodeMatrix(inputEncoded);

            input.Zero();

            for (int x = 0; x < m; x++)
            {
                for (int y = 0; y < n; y++)
                {
                    Assert.AreEqual(expectedOutput[y][x], input[y][x]);
                }
            }
        }
    }
}
