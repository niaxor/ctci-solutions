﻿using System;
using _1._7_RotateMatrix;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace RotateMatrix.Tests
{
    [TestClass]
    public class ArrayExtensionsTests
    {
        private static readonly int[][] nOneMatrix = { new int[] { 1 } };
        private static readonly int[][] nOneMatrixRotated = { new int[] { 1 } };
        private static readonly int[][] nTwoMatrix = { new int[] { 1, 2 }, new int[] { 3, 4 } };
        private static readonly int[][] nTwoMatrixRotated = { new int[] { 3, 1 }, new int[] { 4, 2 } };
        private static readonly int[][] nThreeMatrix = { new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 }, new int[] { 7, 8, 9 } };
        private static readonly int[][] nThreeMatrixRotated = { new int[] { 7, 4, 1 }, new int[] { 8, 5, 2 }, new int[] { 9, 6, 3 } };
        private static readonly int[][] nFourMatrix = { new int[] { 1, 2, 3, 4 }, new int[] { 5, 6, 7, 8 }, new int[] { 9, 10, 11, 12 }, new int[] { 13, 14, 15, 16 } };
        private static readonly int[][] nFourMatrixRotated = { new int[] { 13, 9, 5, 1 }, new int[] { 14, 10, 6, 2 }, new int[] { 15, 11, 7, 3 }, new int[] { 16, 12, 8, 4 } };

        [TestMethod]
        public void Rotate_NIsOne_ExpectedOutput()
        {
            nOneMatrix.Rotate();
            int n = 1;
            for (int x = 0; x < n; x++)
            {
                for (int y = 0; y < n; y++)
                {
                    Assert.AreEqual(nOneMatrixRotated[y][x], nOneMatrix[y][x]);
                }
            }
        }

        [TestMethod]
        public void Rotate_NIsTwo_ExpectedOutput()
        {
            nTwoMatrix.Rotate();
            int n = 2;
            for (int x = 0; x < n; x++)
            {
                for (int y = 0; y < n; y++)
                {
                    Assert.AreEqual(nTwoMatrixRotated[y][x], nTwoMatrix[y][x]);
                }
            }
        }

        [TestMethod]
        public void Rotate_NIsThree_ExpectedOutput()
        {
            nThreeMatrix.Rotate();
            int n = 3;
            for (int x = 0; x < n; x++)
            {
                for (int y = 0; y < n; y++)
                {
                    Assert.AreEqual(nThreeMatrixRotated[y][x], nThreeMatrix[y][x]);
                }
            }
        }

        [TestMethod]
        public void Rotate_NIsFour_ExpectedOutput()
        {
            nFourMatrix.Rotate();
            int n = 4;
            for (int x = 0; x < n; x++)
            {
                for (int y = 0; y < n; y++)
                {
                    Assert.AreEqual(nFourMatrixRotated[y][x], nFourMatrix[y][x]);
                }
            }
        }
    }
}