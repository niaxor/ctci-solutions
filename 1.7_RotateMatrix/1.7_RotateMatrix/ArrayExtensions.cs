﻿using System;

namespace _1._7_RotateMatrix
{
    /// <summary>
    ///  Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes, write a method to rotate the image by 90 degrees. 
    ///  Can you do this in place? 
    ///  
    /// This following is an O(n) solution, done in-place.
    /// </summary>
    public static class ArrayExtensions
    {
        public static void Rotate(this int[][] array)
        {
            int n = array.Length;

            // For each box layer
            for (int depth = 0; depth < n / 2; depth++)
            {
                // Rotate 
                for (int i = depth; i < n - depth - 1; i++)
                {
                    // Left -> Top
                    int top = array[depth][i];
                    array[depth][i] = array[(n - 1) - i][depth];

                    // Bottom -> Left
                    array[(n - 1) - i][depth] = array[(n - 1) - depth][(n - 1) - i];

                    // Right -> Bottom
                    array[(n - 1) - depth][(n - 1) - i] = array[i][(n - 1) - depth];

                    // Top -> Right
                    array[i][(n - 1) - depth] = top;
                }
            }
        }
    }
}
