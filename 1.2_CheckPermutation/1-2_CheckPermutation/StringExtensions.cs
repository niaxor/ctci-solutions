﻿using System;

namespace _1_2_CheckPermutation
{
    /// <summary>
    ///  Given two strings, write a method to decide if one is a permutation of the other.
    /// </summary>
    public static class StringExtensions
    {
        public static bool IsPermutation(this string instance, string value)
        {
            // Length guard
            if (instance.Length != value.Length)
                return false;

            // Sort
            
            char[] instanceChars = instance.ToCharArray();
            char[] valueChars = value.ToCharArray();
            Array.Sort(instanceChars);
            Array.Sort(valueChars);

            // Check
            for (int i = 0; i < instanceChars.Length; i++)
            {
                if (instanceChars[i] != valueChars[i])
                    return false;
            }

            return true;
        }
    }
}
