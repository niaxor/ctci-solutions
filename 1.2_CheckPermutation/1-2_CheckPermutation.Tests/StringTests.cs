﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _1_2_CheckPermutation.Tests
{
    [TestClass]
    public class StringTests
    {
        [TestMethod]
        public void IsPermutation_ABCD_DCBA_True()
        {
            string input1 = "ABCD";
            string input2 = "DCBA";
            Assert.IsTrue(input1.IsPermutation(input2));
        }

        [TestMethod]
        public void IsPermutation_ABCD_dcba_False()
        {
            string input1 = "ABCD";
            string input2 = "dcba";
            Assert.IsFalse(input1.IsPermutation(input2));
        }

        [TestMethod]
        public void IsPermutation_ABCD_ABCD_True()
        {
            string input1 = "ABCD";
            string input2 = "ABCD";
            Assert.IsTrue(input1.IsPermutation(input2));
        }

        [TestMethod]
        public void IsPermutation_EmptyString_EmptyString_True()
        {
            string input1 = "";
            string input2 = "";
            Assert.IsTrue(input1.IsPermutation(input2));
        }

        [TestMethod]
        public void IsPermutation_EmptyString_ABCD_False()
        {
            string input1 = "";
            string input2 = "ABCD";
            Assert.IsFalse(input1.IsPermutation(input2));
        }

        [TestMethod]
        public void IsPermutation_ABCD_ABCDD_False()
        {
            string input1 = "ABCD";
            string input2 = "ABCDD";
            Assert.IsFalse(input1.IsPermutation(input2));
        }
    }
}
