﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ReturnKthToLast.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        [DataRow(new int[] { 1, 2, 3, 4 }, 4, 0)]
        [DataRow(new int[] { 1, 2, 3, 4 }, 3, 1)]
        [DataRow(new int[] { 1, 2, 3, 4 }, 2, 2)]
        [DataRow(new int[] { 1, 2, 3, 4 }, 1, 3)]
        public void ReturnKthToLast_StandardInputs_ReturnsExpectedOutput(int[] inputArr, int output, int k)
        {
            var input = new LinkedList<int>(inputArr);
            Assert.AreEqual(output, input.ReturnKthToLast(k).Value);
        }
    }
}
