﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoopDetection
{
    public class LinkedList<T>
    {
        public LinkedListNode<T> First { get; set; }

        /// <summary>
        /// Returns the node in which a loop begins inside the list
        /// </summary>
        /// <returns></returns>
        public LinkedListNode<T> GetLoopBeginning()
        {
            var slowSearchNode = First?.Next;
            var fastSearchNode = First?.Next?.Next;
            
            // Floyds circle detection algorithm
            while (slowSearchNode != null && fastSearchNode != null)
            {
                if (slowSearchNode == fastSearchNode)
                    break;

                slowSearchNode = slowSearchNode?.Next;
                fastSearchNode = fastSearchNode?.Next?.Next;
            }

            // No loop
            if (slowSearchNode == null || fastSearchNode == null)
            {
                return null;
            }

            return GetLoopBeginning(fastSearchNode);
        }

        /// <summary>
        /// Using a known loop node, find the beginning of the loop
        /// </summary>
        /// <param name="loopNode"></param>
        /// <returns></returns>
        private LinkedListNode<T> GetLoopBeginning(LinkedListNode<T> loopNode)
        {
            var node1 = First;
            var node2 = loopNode;

            // Find root node
            while (node1 != node2)
            {
                node1 = node1.Next;
                node2 = node2.Next;
            }

            return node1;
        }
    }
}