﻿using System;
using LoopDetection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LoopDetection.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        public void GetLoopBeginning_NoLoop_ReturnsNull()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.First = new LinkedListNode<int>(1);
            list.First.Next = new LinkedListNode<int>(2);
            list.First.Next.Next = new LinkedListNode<int>(3);
            Assert.IsNull(list.GetLoopBeginning());
        }

        [TestMethod]
        public void GetLoopBeginning_LoopBeginsAtFirstNode_ReturnsCorrectNode()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.First = new LinkedListNode<int>(1);
            list.First.Next = new LinkedListNode<int>(2);
            list.First.Next.Next = new LinkedListNode<int>(3);
            list.First.Next.Next.Next = new LinkedListNode<int>(4);
            list.First.Next.Next.Next.Next = new LinkedListNode<int>(5);
            list.First.Next.Next.Next.Next.Next = list.First;

            var loopBeginning = list.GetLoopBeginning();

            Assert.AreEqual(list.First, loopBeginning);
        }

        [TestMethod]
        public void GetLoopBeginning_LoopBeginsAtSecondNode_ReturnsCorrectNode()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.First = new LinkedListNode<int>(1);
            list.First.Next = new LinkedListNode<int>(2);
            list.First.Next.Next = new LinkedListNode<int>(3);
            list.First.Next.Next.Next = new LinkedListNode<int>(4);
            list.First.Next.Next.Next.Next = list.First.Next;

            var loopBeginning = list.GetLoopBeginning();

            Assert.AreEqual(list.First.Next, loopBeginning);
        }

        [TestMethod]
        public void GetLoopBeginning_LoopBeginsAtFifthNode_ReturnsCorrectNode()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.First = new LinkedListNode<int>(1);
            list.First.Next = new LinkedListNode<int>(2);
            list.First.Next.Next = new LinkedListNode<int>(3);
            list.First.Next.Next.Next = new LinkedListNode<int>(4);
            list.First.Next.Next.Next.Next = new LinkedListNode<int>(5);
            list.First.Next.Next.Next.Next.Next = list.First.Next.Next.Next.Next;

            var loopBeginning = list.GetLoopBeginning();

            Assert.AreEqual(list.First.Next.Next.Next.Next, loopBeginning);
        }
    }
}
