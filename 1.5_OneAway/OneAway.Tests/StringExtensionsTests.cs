﻿using System;
using _1._5_OneAway;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace OneAway.Tests
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        [DataRow("pale", "ple", true)]
        [DataRow("pales", "pale", true)]
        [DataRow("pale", "bale", true)]
        [DataRow("pale", "bake", false)]
        [DataRow("pale", "pale", true)]
        [DataRow("pale", "pales", true)]
        [DataRow("", "", true)]
        [DataRow("a", "b", true)]
        public void IsOneAway_ExpectedValue(string input1, string input2, bool expectedValue)
        {
            Assert.AreEqual(input1.IsOneAway(input2), expectedValue); 
        }
    }
}
