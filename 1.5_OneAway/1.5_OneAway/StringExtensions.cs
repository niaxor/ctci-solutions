﻿using System;
using System.Text;

namespace _1._5_OneAway
{
    /// <summary>
    /// There are three types of edits that can be performed on strings: insert a character, remove a character, or replace a character. 
    /// Given two strings, write a function to check if they are one edit (or zero edits) away. 
    /// 
    /// This is an O(n) solution.
    /// </summary>
    public static class StringExtensions
    {
        public static bool IsOneAway(this string instance, string value)
        {
            // Length difference must not be greater than 1
            if (Math.Abs(instance.Length - value.Length) > 1)
                return false;
            if (instance == value)  // Same
                return true;
            if (IsOneReplaceAway(instance, value)) // Replace
                return true;
            if (IsOneInsertAway(instance, value)) // Insert / Remove
                return true;

            return false;
        }

        public static bool IsOneReplaceAway(string value1, string value2)
        {
            // Length must be the same
             if (Math.Abs(value1.Length - value2.Length) != 0)
                return false;

            int replacements = 0;
            // Build a version of value1 with a single replaced character
            for (int i = 0; i < Math.Max(value1.Length, value2.Length); i++)
            {
                // Found a character that needs a replacement
                if (value1[i] != value2[i])
                {
                    replacements++;

                    if (replacements > 1)
                        return false;
                }
            }

            return replacements < 2;
        }

        public static bool IsOneInsertAway(string value1, string value2)
        {
            // Length must be off-by-one
            if (Math.Abs(value1.Length - value2.Length) != 1)
                return false;

            var shorterString = value1.Length > value2.Length ? value2 : value1;
            var longerString = value1.Length > value2.Length ? value1 : value2;
            int inserts = 0;
            int shortIndex = 0;
            for (int longIndex = 0; longIndex < longerString.Length; longIndex++)
            {
                if (shortIndex >= shorterString.Length || shorterString[shortIndex] != longerString[longIndex])
                {
                    inserts++;
                    longIndex++;
                }

                longIndex++;

                if (inserts > 1)
                    return false;
            }

            return inserts < 2;
        }
    }
}