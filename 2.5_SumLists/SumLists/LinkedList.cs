﻿using System;
using System.Collections.Generic;

namespace SumLists
{
    public class LinkedList
    {
        public LinkedListNode<int> First { get; set; }
        public LinkedListNode<int> Last { get; set; }

        public LinkedList() { }

        public LinkedList(int[] collection) 
        {
            var node = new LinkedListNode<int>();
            foreach (var item in collection)
            {
                if (First == null)
                {
                    First = new LinkedListNode<int>() { Value = item };
                    node = First;
                }
                else
                {
                    node.Next = new LinkedListNode<int>() { Value = item };
                    node.Next.Previous = node;
                    node = node.Next;
                }
            }
            Last = node;
        }

        public static LinkedList SumLists(LinkedList list1, LinkedList list2)
        {
            LinkedList result = new LinkedList();
            LinkedListNode<int> currentResultNode = null;
            var node1 = list1.First;
            var node2 = list2.First;
            int carry = 0;

            while (node1 != null || node2 != null)
            {
                // Calculate result
                int value1 = node1 != null ? node1.Value : 0;
                int value2 = node2 != null ? node2.Value : 0;
                int sumResult = value1 + value2 + carry;

                if (result.First == null)
                {
                    currentResultNode = new LinkedListNode<int>();
                    result.First = currentResultNode;
                }
                else
                {
                    currentResultNode.Next = new LinkedListNode<int>();
                    currentResultNode.Next.Previous = currentResultNode;
                    currentResultNode = currentResultNode.Next;
                }
                currentResultNode.Value = sumResult % 10;
                carry = sumResult / 10;

                // Iterate nodes
                node1 = node1?.Next;
                node2 = node2?.Next;
            }

            // Extra carry digit
            if (carry != 0)
            {
                currentResultNode.Next = new LinkedListNode<int>();
                currentResultNode.Next.Previous = currentResultNode;
                currentResultNode = currentResultNode.Next;
                currentResultNode.Value = carry;
            }

            result.Last = currentResultNode;
            return result;
        }

        public static LinkedList SumListsForwardOrder(LinkedList list1, LinkedList list2)
        {
            LinkedList result = new LinkedList();
            LinkedListNode<int> currentResultNode = null;
            var node1 = list1.Last;
            var node2 = list2.Last;
            int carry = 0;

            while (node1 != null || node2 != null)
            {
                // Calculate result
                int value1 = node1 != null ? node1.Value : 0;
                int value2 = node2 != null ? node2.Value : 0;
                int sumResult = value1 + value2 + carry;

                if (result.Last == null)
                {
                    currentResultNode = new LinkedListNode<int>();
                    result.Last = currentResultNode;
                }
                else
                {
                    currentResultNode.Previous = new LinkedListNode<int>();
                    currentResultNode.Previous.Next = currentResultNode;
                    currentResultNode = currentResultNode.Previous;
                }
                currentResultNode.Value = sumResult % 10;
                carry = sumResult / 10;

                // Iterate nodes
                node1 = node1?.Previous;
                node2 = node2?.Previous;
            }

            // Extra carry digit
            if (carry != 0)
            {
                currentResultNode.Previous = new LinkedListNode<int>();
                currentResultNode.Previous.Next = currentResultNode;
                currentResultNode = currentResultNode.Previous;
                currentResultNode.Value = carry;
            }

            result.First = currentResultNode;
            return result;
        }

        public override bool Equals(object obj)
        {
            // Must be linked list
            if (!(obj is LinkedList))
                return false;

            var listToCheck = obj as LinkedList;
            // Check for non-matching First/Last properties
            if (!Object.Equals(this.First, listToCheck.First) || !Object.Equals(this.Last, listToCheck.Last))
                return false;

            var node1 = this.First;
            var node2 = listToCheck.First;

            // Check each nodes value is identical 
            while (node1 != null && node2 != null)
            {
                if (!node1.Value.Equals(node2.Value))
                    return false;

                node1 = node1.Next;
                node2 = node2.Next;
            }

            // Check length is equal
            return node1 == null && node2 == null;
        }

        public override int GetHashCode()
        {
            // Hashing performance is irrelevant in this example
            return 0;
        }
    }
}
