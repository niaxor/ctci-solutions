﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SumLists.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        [DataRow(new int[] { 1 }, new int[] { 1 }, new int[] { 2 })]
        public void SumLists_SingleDigit_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumLists(list1, list2));
        }

        [TestMethod]
        [DataRow(new int[] { 9 }, new int[] { 4 }, new int[] { 3, 1 })]
        [DataRow(new int[] { 9, 0, 1 }, new int[] { 2 }, new int[] { 1, 1, 1 })]
        public void SumLists_Carry_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumLists(list1, list2));
        }

        [TestMethod]
        [DataRow(new int[] { 1, 1 }, new int[] { 2, 2 }, new int[] { 3, 3 })]
        [DataRow(new int[] { 0, 0, 0, 1 }, new int[] { 5, 5, 5, 8 }, new int[] { 5, 5, 5, 9 })]
        public void SumLists_MultipleDigits_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumLists(list1, list2));
        }

        [TestMethod]
        [DataRow(new int[] { 1 }, new int[] { 1, 1 }, new int[] { 2, 1 })]
        public void SumLists_DifferentDigits_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumLists(list1, list2));
        }

        [TestMethod]
        [DataRow(new int[] { 1 }, new int[] { 1 }, new int[] { 2 })]
        public void SumListsForwardOrder_SingleDigit_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumListsForwardOrder(list1, list2));
        }

        [TestMethod]
        [DataRow(new int[] { 9 }, new int[] { 4 }, new int[] { 1, 3 })]
        [DataRow(new int[] { 9, 0, 1 }, new int[] { 2 }, new int[] { 9, 0, 3 })]
        public void SumListsForwardOrder_Carry_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumListsForwardOrder(list1, list2));
        }

        [TestMethod]
        [DataRow(new int[] { 1, 1 }, new int[] { 2, 2 }, new int[] { 3, 3 })]
        [DataRow(new int[] { 0, 0, 0, 1 }, new int[] { 5, 5, 5, 8 }, new int[] { 5, 5, 5, 9 })]
        public void SumListsForwardOrder_MultipleDigits_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] resultData)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(resultData);
            var result = LinkedList.SumListsForwardOrder(list1, list2);
            Assert.AreEqual(expectedResult, result);
        }

        [TestMethod]
        [DataRow(new int[] { 1 }, new int[] { 1, 1 }, new int[] { 1, 2 })]
        public void SumListsForwardOrder_DifferentDigits_ReturnsExpectedResult(int[] list1Data, int[] list2Data, int[] result)
        {
            var list1 = new LinkedList(list1Data);
            var list2 = new LinkedList(list2Data);
            var expectedResult = new LinkedList(result);
            Assert.AreEqual(expectedResult, LinkedList.SumListsForwardOrder(list1, list2));
        }
    }
}
