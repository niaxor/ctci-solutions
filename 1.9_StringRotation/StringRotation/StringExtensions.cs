﻿namespace StringRotation
{
    public static class StringExtensions
    {
        /// <summary>
        ///  Assume you have a method isSubst ring which checks if one word is a substring of another. 
        ///  Given two strings, 51 and 52, write code to check if 52 is a rotation of 51 using only one call to isSubstring 
        ///  (e.g., "waterbottle" is a rotation of"erbottlewat"). 
        ///  
        /// This is an O(n) solution
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static bool IsRotation(this string instance, string value)
        {
            if (instance == value)
                return true;
            if (instance.Length != value.Length)
                return false;

            // e.g. erbottlewaterbottlewat <-- will always contain all rotations
            string doubleValue = value + value;
            // Contains is the 'IsSubstring' equivelant in C#
            return doubleValue.Contains(instance);
        }
    }
}