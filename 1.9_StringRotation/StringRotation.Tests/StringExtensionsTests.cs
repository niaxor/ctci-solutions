﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringRotation.Tests
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        [DataRow("12345", "34512", true)]
        [DataRow("12345", "23451", true)]
        [DataRow("12345", "12345", true)]
        [DataRow("12345", "51234", true)]
        [DataRow("1", "1", true)]
        [DataRow("", "", true)]
        [DataRow("ab", "ba", true)]
        [DataRow("abc", "bca", true)]
        [DataRow("12345", "35512", false)]
        [DataRow("12345", "", false)]
        [DataRow("12345", "a", false)]
        [DataRow("12345", "123", false)]
        [DataRow("123", "12345", false)]
        [DataRow("12345", "54321", false)]
        [DataRow("12345", "123456", false)]
        [DataRow("12345", "012345", false)]
        public void IsRotation_ExpectedResult(string input1, string input2, bool result)
        {
            Assert.AreEqual(result, input1.IsRotation(input2));
        }
    }
}
