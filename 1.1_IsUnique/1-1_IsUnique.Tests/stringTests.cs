﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace _1_1_IsUnique.Tests
{
    [TestClass]
    public class StringTests
    {
        [TestMethod]
        public void IsUnique_AA_False()
        {
            string input = "AA";
            Assert.IsFalse(input.IsUnique());
        }

        [TestMethod]
        public void IsUnique_ABC_True()
        {
            string input = "ABC";
            Assert.IsTrue(input.IsUnique());
        }

        [TestMethod]
        public void IsUnique_AaAa_False()
        {
            string input = "AaAa";
            Assert.IsFalse(input.IsUnique());
        }

        [TestMethod]
        public void IsUnique_Aa_True()
        {
            string input = "Aa";
            Assert.IsTrue(input.IsUnique());
        }
    }
}
