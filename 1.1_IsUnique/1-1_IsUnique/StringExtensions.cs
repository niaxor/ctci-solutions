﻿namespace _1_1_IsUnique
{
    /// <summary>
    ///  Implement an algorithm to determine if a string has all unique characters. What if you cannot use additional data structures?
    /// </summary>
    public static class StringExtensions
    {
        public static bool IsUnique(this string s)
        {
            char[] characters = s.ToCharArray();
            for (int i = 0; i < characters.Length; i++)
            {
                for (int j = i + 1; j < characters.Length; j++)
                {
                    if (s[i] == s[j])
                        return false;
                }
            }

            return true;
        }
    }
}