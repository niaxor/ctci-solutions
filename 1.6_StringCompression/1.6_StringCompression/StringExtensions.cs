﻿using System.Text;

namespace _1._6_StringCompression
{
    /// <summary>
    ///  Implement a method to perform basic string compression using the counts of repeated characters. 
    ///  For example, the string aabcccccaaa would become a2b1c5a3. 
    ///  If the "compressed" string would not become smaller than the original string, your method should return the original string. 
    ///  You can assume the string has only uppercase and lowercase letters (a - z). 
    ///  
    /// The following implementation is an O(n) solution to the above problem.
    /// </summary>
    public static class StringExtensions
    {
        public static string Compress(this string instance)
        {
            // Create compressed representation
            StringBuilder compressedOutputBuilder = new StringBuilder();
            int i = 0;
            while (i < instance.Length)
            {
                var currentCharacter = instance[i];
                int characterCount = 1;

                // Look ahead to count currrent characters
                while (i + characterCount < instance.Length)
                {
                    if (instance[i + characterCount] == currentCharacter)
                        characterCount++;
                    else
                        break;
                }

                compressedOutputBuilder.Append(currentCharacter);
                compressedOutputBuilder.Append(characterCount);
                i += characterCount;
            }
            string compressedOutput = compressedOutputBuilder.ToString();

            // Return shortest string out of [compressed, raw]
            return compressedOutput.Length < instance.Length ? compressedOutput : instance;
        }
    }
}
