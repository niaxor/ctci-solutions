﻿using System;
using _1._6_StringCompression;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace StringCompression.Tests
{
    [TestClass]
    public class StringExtensionsTests
    {
        [TestMethod]
        [DataRow("abc", "abc")]
        [DataRow("aabbcc", "aabbcc")]
        [DataRow("aaabbbccc", "a3b3c3")]
        [DataRow("", "")]
        [DataRow("aaaaaaaaaa", "a10")]
        [DataRow("aAaA", "aAaA")]
        [DataRow("AAAA", "A4")]
        [DataRow("aaaaAAAAaaaaAAAA", "a4A4a4A4")]
        public void Compress_ExpectedValue(string input, string expectedOutput)
        {
            Assert.AreEqual(expectedOutput, input.Compress());
        }
    }
}
