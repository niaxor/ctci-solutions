﻿namespace RemoveDups
{
    public class LinkedListNode<T>
    {
        public T Value { get; set; }
        public LinkedListNode<T> Next { get; set; }

        public override bool Equals(object obj)
        {
            var node = obj as LinkedListNode<T>;
            if (node == null)
                return false;

            return Value.Equals(node.Value);
        }

        public override int GetHashCode()
        {
            if (Value == null)
                return 0;

            return Value.GetHashCode();
        }
    }
}