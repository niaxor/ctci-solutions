﻿using System.Collections.Generic;

namespace RemoveDups
{
    public static class LinkedListExtensions
    {
        public static void RemoveDups<T>(this LinkedList<T> list)
        {
            HashSet<T> values = new HashSet<T>();
            var node = list.First;
            var previousNode = list.First;
            if (node == null || node.Next == null)
                return;

            values.Add(node.Value);
            node = node.Next;
            while (node != null)
            {
                if (!values.Add(node.Value))
                {
                    previousNode.Next = node.Next;
                }
                else
                {
                    previousNode = node;
                }

                node = node.Next;
            }

            list.Last = previousNode;
        }

        public static void RemoveDupsUnbuffered<T>(this LinkedList<T> list)
        {
            if (list.First == null || list.First.Next == null)
                return;

            LinkedListNode<T> referenceNode = list.First;
            LinkedListNode<T> currentNode;
            LinkedListNode<T> previousNode;

            // Iterate over unique values
            while (referenceNode != null)
            {
                currentNode = referenceNode.Next;
                previousNode = referenceNode;
                while (currentNode != null)
                {
                    // Remove a duplicate
                    if (currentNode.Value.Equals(referenceNode.Value))
                    {
                        previousNode.Next = currentNode.Next;
                    }
                    else
                    {
                        previousNode = currentNode;
                    }

                    currentNode = currentNode.Next;
                }

                // Update last value
                if (referenceNode.Next == null)
                {
                    list.Last = referenceNode;
                }

                referenceNode = referenceNode.Next;
            }
        }
    }
}
