﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

namespace RemoveDups.Tests
{
    [TestClass]
    public class LinkedListExtensionsTests
    {
        [TestMethod]
        [DataRow(new int[] { 1, 1 }, new int[] { 1 })]
        [DataRow(new int[] { 0, 0 } , new int[] { 0 })]
        [DataRow(new int[] { 1, 1, 1, 1, 1, 1 }, new int[] { 1 })]
        [DataRow(new int[] { 1, 1, 2, 2, 3, 3, 4, 4}, new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { 1, 2, 3, 4, 4 }, new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { 1, 2, 3, 3, 4 }, new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { 1, 1, 2, 3, 4}, new int[] { 1, 2, 3, 4 })]
        public void RemoveDups_Duplicates_DuplicatesRemoved(int[] inputArr, int[] outputArr)
        {
            var input = new LinkedList<int>(inputArr);
            var output = new LinkedList<int>(outputArr);
            input.RemoveDups();
            Assert.AreEqual(output, input);
        }

        [TestMethod]
        [DataRow(new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { })]
        [DataRow(new int[] { 0 })]
        [DataRow(new int[] { 1, 11, 111 })]
        public void RemoveDupsUnbuffered_NoDuplicates_ResultMatchesInput(int[] inputValues)
        {
            var dupsRemovedList = new LinkedList<int>(inputValues);
            var untouchedList = new LinkedList<int>(inputValues);
            dupsRemovedList.RemoveDupsUnbuffered();
            Assert.AreEqual(untouchedList, dupsRemovedList);
        }

        [TestMethod]
        [DataRow(new int[] { 1, 1 }, new int[] { 1 })]
        [DataRow(new int[] { 0, 0 }, new int[] { 0 })]
        [DataRow(new int[] { 1, 1, 1, 1, 1, 1 }, new int[] { 1 })]
        [DataRow(new int[] { 1, 1, 2, 2, 3, 3, 4, 4 }, new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { 1, 2, 3, 4, 4 }, new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { 1, 2, 3, 3, 4 }, new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { 1, 1, 2, 3, 4 }, new int[] { 1, 2, 3, 4 })]
        public void RemoveDupsUnbuffered_Duplicates_DuplicatesRemoved(int[] inputArr, int[] outputArr)
        {
            var input = new LinkedList<int>(inputArr);
            var output = new LinkedList<int>(outputArr);
            input.RemoveDupsUnbuffered();
            Assert.AreEqual(output, input);
        }

        [TestMethod]
        [DataRow(new int[] { 1, 2, 3, 4 })]
        [DataRow(new int[] { })]
        [DataRow(new int[] { 0 })]
        [DataRow(new int[] { 1, 11, 111 })]
        public void RemoveDups_NoDuplicates_ResultMatchesInput(int[] inputValues)
        {
            var dupsRemovedList = new LinkedList<int>(inputValues);
            var untouchedList = new LinkedList<int>(inputValues);
            dupsRemovedList.RemoveDups();
            Assert.AreEqual(untouchedList, dupsRemovedList);
        }
    }
}
