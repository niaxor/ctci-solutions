﻿using System;
using System.Collections.Generic;

namespace Palindrome
{
    public class LinkedList<T>
    {
        public LinkedListNode<T> First { get; set; }
        public LinkedListNode<T> Last { get; set; }

        public LinkedList() { }

        public LinkedList(T[] collection) 
        {
            var node = new LinkedListNode<T>();
            foreach (var item in collection)
            {
                if (First == null)
                {
                    First = new LinkedListNode<T>() { Value = item };
                    node = First;
                }
                else
                {
                    node.Next = new LinkedListNode<T>() { Value = item };
                    node.Next.Previous = node;
                    node = node.Next;
                }
            }
            Last = node;
        }

        public bool IsPalindrome()
        {
            var forwardNode = First;
            var reverseNode = Last;

            if (forwardNode == null && reverseNode == null)
                return true;

            while (forwardNode != null && reverseNode != null &&
                forwardNode != reverseNode)
            {
                if (!forwardNode.Equals(reverseNode))
                {
                    return false;
                }

                // Check if forward node and reverse node have caught up (in even list)
                if (forwardNode.Next == reverseNode)
                {
                    return true;
                }

                forwardNode = forwardNode.Next;
                reverseNode = reverseNode.Previous;
            }

            return true;
        }

        public override bool Equals(object obj)
        {
            // Must be linked list
            if (!(obj is LinkedList<T>))
                return false;

            var listToCheck = obj as LinkedList<T>;
            // Check for non-matching First/Last properties
            if (!Object.Equals(this.First, listToCheck.First) || !Object.Equals(this.Last, listToCheck.Last))
                return false;

            var node1 = this.First;
            var node2 = listToCheck.First;

            // Check each nodes value is identical 
            while (node1 != null && node2 != null)
            {
                if (!node1.Value.Equals(node2.Value))
                    return false;

                node1 = node1.Next;
                node2 = node2.Next;
            }

            // Check length is equal
            return node1 == null && node2 == null;
        }

        public override int GetHashCode()
        {
            // Hashing performance is irrelevant in this example
            return 0;
        }
    }
}
