﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Palindrome.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        [DataRow(new int[] { })]
        public void IsPalindrome_NullData_ReturnsTrue(int[] input)
        {
            var linkedList = new LinkedList<int>(input);
            Assert.IsTrue(linkedList.IsPalindrome());
        }

        [TestMethod]
        [DataRow(new int[] { 1, 1 })]
        [DataRow(new int[] { 2, 2, 2, 2 })]
        [DataRow(new int[] { 3, 3, 3, 3, 3, 3 })]
        public void IsPalindrome_Even_ReturnsTrue(int[] input)
        {
            var linkedList = new LinkedList<int>(input);
            Assert.IsTrue(linkedList.IsPalindrome());
        }

        [TestMethod]
        [DataRow(new int[] { 1 })]
        [DataRow(new int[] { 2, 2, 2 })]
        [DataRow(new int[] { 3, 3, 3, 3, 3 })]
        public void IsPalindrome_Odd_ReturnsTrue(int[] input)
        {
            var linkedList = new LinkedList<int>(input);
            Assert.IsTrue(linkedList.IsPalindrome());
        }

        [TestMethod]
        [DataRow(new int[] { 1 })]
        [DataRow(new int[] { 2, 3, 2 })]
        [DataRow(new int[] { 3, 1, 5, 1, 3 })]
        [DataRow(new int[] { 5, 1, 1, 5 })]
        [DataRow(new int[] { 3, 2, 3 })]
        [DataRow(new int[] { 9, 3, 3, 9,  })]
        public void IsPalindrome_VaryingIntegers_ReturnsTrue(int[] input)
        {
            var linkedList = new LinkedList<int>(input);
            Assert.IsTrue(linkedList.IsPalindrome());
        }
    }
}
